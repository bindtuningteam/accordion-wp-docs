### BindTuning Accordion browser compatibility

The latest stable version includes support for the following versions: 

| ![Chrome](https://raw.github.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png) | ![Firefox](https://raw.github.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png) | ![IE](https://raw.github.com/alrra/browser-logos/master/src/archive/internet-explorer_9-11/internet-explorer_9-11_48x48.png) | ![Edge](https://raw.github.com/alrra/browser-logos/master/src/edge/edge_48x48.png) | ![Opera](https://raw.github.com/alrra/browser-logos/master/src/opera/opera_48x48.png) | ![Safari](https://raw.github.com/alrra/browser-logos/master/src/safari/safari_48x48.png) |
| --- | --- | --- | --- | --- | --- |
| <span style="color:green">Latest ✔</span> | <span style="color:green">Latest ✔</span> | <span style="color:red">Not Supported</span> | <span style="color:green">Latest ✔</span> | <span style="color:green">Latest ✔</span> | <span style="color:green">Latest ✔</span> |

As of version 1.0.10.31 BindTuning Accordion for SharePoint is no longer compatible with IE11 or less.

As Microsoft 365 evolves, we continually evaluate our apps and services to make sure that we deliver the most value to customers. On August 2021, Microsoft announced that in one year they would say farewell to Internet Explorer 11.

___

### Microsoft 365 apps say farewell to Internet Explorer 11

![browser-support](../images/browsersupport.png)

_August 17, 2021: Support is now unavailable for Microsoft 365 apps and services on IE11. Additionally, you should expect no new features when accessing Microsoft 365 apps and services on IE11 and that the daily usage experience for users could get progressively worse over time until the apps and services are disconnected. Banners will be used to communicate and alert users to upcoming changes in experience, such as app or service disconnection and/or redirection._

 
_July 23, 2021: Beginning August 17, 2021, Microsoft 365 apps and services will no longer support Internet Explorer 11 (IE11) and users may have a degraded experience, or be unable to connect to, those apps and services. These apps and services will phase out over weeks and months to ensure a smooth end of support, with each app and service phasing out on independent schedules._


_Focusing on Microsoft Edge accelerate innovation in Microsoft 365 experiences from the browser and in modern apps, such as Microsoft Teams, OneDrive, SharePoint, Lists, and more._

_Beginning on August 17, 2021, Microsoft 365 apps and services will no longer support Internet Explorer 11. While we know this change will be difficult for some customers, we believe that you'll get the most out of Microsoft 365 when using Microsoft Edge or other modern browsers._


#### Usefull links

* [Microsoft 365 apps say farewell to Internet Explorer 11 and Windows 10 sunsets Microsoft Edge Legacy](https://techcommunity.microsoft.com/t5/microsoft-365-blog/microsoft-365-apps-say-farewell-to-internet-explorer-11-and/ba-p/1591666)
* [Prepare your SharePoint environment for the retirement of Internet Explorer 11 for Microsoft 365 apps and services](https://docs.microsoft.com/en-us/sharepoint/prepare-IE11)

#### Technical articles
* [Internet Explorer 11 performance with SPFx](https://github.com/SharePoint/sp-dev-docs/issues/1438)
* [Performance and memory leaks in modern pages and SPFx](https://github.com/SharePoint/sp-dev-docs/issues/2435)