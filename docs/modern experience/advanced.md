![Add section](../images/modern/03.advancedoptions.png)

### Title Link 

Note this option is only valid if you have a Title on the Web Part.<br>
On the **Title** of the Web Part, you can add an URL to redirect you to any page. The Web Part will always open the new URL in a new tab of your browser and that option is only visible if you're not in **Edit** mode.

___
### Always Show BT Buttons

When this is **turned on** the icons to manage the content of the Web Part are always visible when the page is in edit mode.<br>
When the feature is **turned off** you need to hover the web part to see the controls.

___
### Title

If you click on the Title of the Web Part on the page, you can change the Title to the Web Part that will be shown. When you insert a text there it'll show on the page the **Title**, otherwise, nothing will be visible in the page.

![02.appearance.text.png](../images/modern/02.appearance.text.png)

___
### Allow HTML
 
If you activate this option, the item's content will be displayed as HTML instead of plain text. This is useful if you want to insert your custom html or an embedded widget.

<p class="alert alert-warning"><strong>Warning:</strong> Malformed HTML can lead to a broken page layout. If that happens, you may not be able to edit the web part and you need to go to the list and change (or delete) the HTML content. An other way to solve a broken page layout is to open the page in maintenance mode and remove the web part from the page. To do that, just append the flag <strong>?maintenancemode=true</strong> at the end of your page URL.</p>
 
 ![maintenance](../images/modern/12.maintenancemode.png)