![06.options](../images/modern/08.createlist.png)

Type the name of the list that you want to create and click **Create the List**;

The list will be created on your current Site and connected with the Web Part;

If you intend to edit the list connection go to the [Sources](./list.md)
