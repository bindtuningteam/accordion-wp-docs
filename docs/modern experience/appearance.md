![webpartapperence](../images/modern/06.webpartapperence.png)

### Theme Colors on the Title Bar

Enabling this option will make it so the web part's title bar inherits the background color from your theme or customization on the page. 
This option should help in maintaining branding consistency with native SharePoint web parts.

___
### Theme Colors on the WebPart Content

Apply the Styles of the theme to the Accordion.

___
### Right to Left

Using this option will change the web part's text orientation to go from right to left. 
The forms will not be affected.