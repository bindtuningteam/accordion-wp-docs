1. Click on the **Edit** button;

	![edit-page](../../images/modern/01.edit.modern.png)


2. Mouse hover the Web Part and click on the **[+]** button to add a new **Accordion's item**;

	![add_faq](../../images/modern/02.add_faq.gif)
	

3. Fill out the form that pops up. You can check out what you need to do in each setting in the [Accordion Settings](../../global/faq)

4. After setting everything up, click on **Save**. 