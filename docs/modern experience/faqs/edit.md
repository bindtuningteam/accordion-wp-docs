1. Click on the **Edit** button;

	![edit-page](../../images/modern/01.edit.modern.png)

2. On the web part click the **Manage Accordion** icon;
3. The list of items will appear. Click the **pencil** icon to edit the item;
4. You can check what you can edit in each section on the [Accordion Settings](../../global/faq); 

	![edit-faq](../../images/modern/03.edit_faq.gif)

5. Done editing? Click on **Save** to save your settings.