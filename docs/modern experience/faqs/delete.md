1. Click on the **Edit** button;

    ![edit-page](../../images/modern/01.edit.modern.png)

2. On the web part click the **Manage Accordion** icon;
3. The list of items will appear. Click the **trash** icon to delete the item;
4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the item will be removed.

    ![delete_faq](../../images/modern/04.delete_faq.gif)