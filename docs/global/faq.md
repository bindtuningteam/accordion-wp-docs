### Order

First up is selecting your new item's order. This order is based on a numerical order.

    - 0 - this item will be the first one to appear
    - 10 - this item will be the second one to appear
    - 20 - this item will be the third one to appear
    - ...

<p class="alert alert-info">Items with the same value are ordered based on date of creation (older item first). We recommend to use increments of 10 so then you can add items on middle easily. These values will be used if you choose <strong>Sequence</strong> in Sorting Options of the web part properties. Also the order may be inverted depending on the selected direction (Ascendant/Descendant)   </p>

___
### Title (mandatory)

This is the title that will appear on the item's header

___
### Image

Here, you can select an Image or an Icon. If you choose an image, insert the image's URL or use the image browser button, for more informtion check the [next link](./image). If you choose an icon just select one from the icon's list.

___
### Status

This toggles the item's visibilty on/off

___
### Accordion

You can choose if the item will appear collapsed or not

___
### Content (mandatory)

This is the item's content, that can be displayed as plain text or HTML, depending if the HTML option is activated, or not, on the web part properties.

![html-content](../images/classic/34.htmlcontent.png)
