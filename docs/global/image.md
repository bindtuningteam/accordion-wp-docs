When you select the option to be an image it will allow you to:

![background](../images/classic/35.image_picker.png)

- **Insert an URL** of the image location directly to the text area;
- **Browse on your SharePoint site** and pick a picture. If you click on **Browse** that will open a panel where you can select an image from your SharePoint libraries. 

![image_picker](../images/classic/36.image_picker.png)

___
### Upload image

You can also upload your image for the select library by selecting the icon at the top to **Upload File** or by **dragging the image** for the area. Note this will upload the image directly to Library that you've selected. 