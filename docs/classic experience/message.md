![Messages](../images/classic/06.messages.png)

Decide how you would like the web part errors to be displayed. Three options are available
 
- **Show In Edit Mode Only** - Error messages will be visible only if the page is in edit mode.
- **Always Show** - Error messages will always be visible to any user.
- **Never Show** - Error messages will never be visible.
