### Create a list ###

This list is what you will use to store your Accordion data. Let's create your list:

1. Click on Settings ⚙️ and then on Add an app;

    ![add-list](../images/classic/21.addalist.png)

2. On the search box type BT, look for the Accordion BT and click;

    ![create-list](../images/classic/23.createlist.gif)

3. Name the list and click Create. List created;

### Accordion List ###

Here you will need to connect your web part with a SharePoint list. Without it, the web part will not function and an notification message will appear. Let's get to it:

![list-picker](../images/classic/22.listpicker.png)

1. On **Accordion Lists**, click **Add List**

2. On the first text box insert one of the two:

 - **The URL of the list you created**
 - **A URL of a site collection**

    <p class="alert alert-warning">Use relative paths for this field. So instead of using an URL like <strong>"https://company.sharepoint.com/sites/Home”</strong>, you should use something like <strong>“/sites/Home/”</strong>.</p>

3. On the second box, select the list you have created to be used if not already selected;

4. On Filtering Options section, select how you want to filter your items:

    ![filtering-options](../images/classic/23.filteringoptions.png)

    - No Filters - All the items from your list will be retrieved and displayed on the web part.

    - CAML Query - This advanced option gives you full control over how you filter your results. Selecting this option, a text field appears to write your own CAML Query. We recommend the use of the free to use tool [U2U Caml Query Builder](https://www.u2u.be/software/), to help write CAML Queries.

    - Other Options - Under the first 2 options, your list views will be listed.

    - On the Field Mappings section, map the fields from the list with the fields of the web part

    You will find dropdowns for each of the web part's fields. These dropdowns include all the columns in your list's selected view. Select which column corresponds to which field or select No Mapping when nothing fits the bill. If you want to repeat the same column for different fields that's ok too.

5. When you're done, lock your selection by clicking the save icon.

