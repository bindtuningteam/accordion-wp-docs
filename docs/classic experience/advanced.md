![insert-tab](../images/classic/32.advancedoptions.png)

___
### Allow HTML
 
If you activate this option, the item's content will be displayed as HTML instead of plain text. This is useful if you want to insert your custom html or an embedded widget.

<p class="alert alert-warning"><strong>Warning:</strong> Malformed HTML can lead to a broken page layout. If that happens, you may not be able to edit the web part and you need to go to the list and change (or delete) the HTML content. An other way to solve a broken page layout is to open the page in maintenance mode and remove the web part from the page. To do that, just append the flag <strong>?contents=1</strong> at the end of your page URL.</p>
 
![maintenance](../images/classic/33.maintenance.png)

___
### Language

From here you can select which language you want to be displayed by the web part. This will also translate the forms, but a refresh may be required before you see the changes.

By default, the BindTuning Web Part provides the language for:

- English
- Portuguese

If you'd like to localize the web part to your own language, please refer to [this article](https://support.bindtuning.com/hc/en-us/articles/115004585263).