1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. On the web part sidebar and click the **Manage Accordion** icon;
3. The list of items will appear. Click the **trash** icon to delete the item;
4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the item will be removed.	

    ![delete_faqs](../../images/classic/03.delete_faq.gif)