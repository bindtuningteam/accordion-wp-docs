1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. On the web part sidebar and click the **Manage Accordion** icon;
3. The list of items will appear. Click the **Edit** icon to edit the item;
4. You can check what you can edit in each section on the [Accordion Settings](../../global/faq);

	![edit_item](../../images/classic/04.edit_faq.gif)

5. Done editing? Click on **Save** to save your settings.

	![save_changes](../../images/classic/16.save.png)