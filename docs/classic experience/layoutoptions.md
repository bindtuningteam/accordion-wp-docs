![layout-options](../images/classic/26.layoutoptions.png)

___
### Layout
There are three different layouts you can choose from


|Layout1|Layout2|Layout3|
|----------|-------------|------|
|![layout-1](../images/classic/27.layout1.png)|![layout-2](../images/classic/28.layout2.png)|![layout-3](../images/classic/29.layout3.png)|

___
### Header Text Color ###
The color of item's header text.

___
### Header Contrast Color ###
The secondary color of the item's header. This is available on Layout 1 only. The other two layouts use a calculated darker or lighter variant of the text color.

___
### Show Search ###
Activate this option if you want to display a search bar on top of the webpart. This search, allows users to find and filter items that contain the search expression.

___
### Transparency ###
If the item's content background should be transparent or not. If not, the background will be white. 

___
Layout Examples:

|Opaque|Transparent|
|----------|-------------|
|![opaque](../images/classic/30.opaque.png)|![transparent](../images/classic/31.transparent.png)|


|![search](../images/classic/32.search.gif)