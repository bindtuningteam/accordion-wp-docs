On the Web Part Properties panel, you've multiple options which you can edit for different configuration of the Web Part.
 
- [List Settings](../listsettings)
- [Sorting Options](../sortingoptions)
- [Layout Settings](../layoutoptions)
- [Advanced Options](../advanced)
- [Performance](../performance)
- [Web Part Appearance](../appearance)
- [Web Part Messages](../message)

![generaloptions](../images/classic/01.generaloptions.gif)

The global settings form let you apply options to **all the web parts** on the page at once. To use the form, follow the steps:

- [Configure Global Settings](../globalsettings)

![globalsettings](../images/classic/05.globalsettings.png)