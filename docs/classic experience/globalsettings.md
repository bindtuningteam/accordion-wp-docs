### Configure Global Settings
 
The global settings form lets you apply options to all the web parts on the page at once. To use the form, follow these steps:

1. On the ribbon, click on the BindTuning tab 

![globalsettings](../images/classic/05.globalsettings.png)

2. Click on the Settings button in the Web Parts Panel
3. Set the options as you see fit (you can find descriptions for these fields below)
4. Hit the save button and wait for the form to close

![globalsettings](../images/classic/15.globalsettingsmodal.png)

___
### Performance Options

**Update cache for up to x hours**

<p class="alert alert-success">This option will only apply to web parts using Local Storage Cache.</p>

When you activate local storage caching on your web part, it will start checking for any list changes every 30 minutes behind the scenes. If any changes are detected, the cache will be updated and next time you come into the page the content will be current.

To reduce the amount of requests done to SharePoint's API, the web part will only do these checkes for a certain amount of time. After this time is expired, the checks will cease and a message will be presented to the user to confirm that he is not away and the checks should resume.

By default the option is set to 4 hours but here you can extend or reduce the period as you see fit.

**Group requests**

The BindTuning web parts need to request a lot of information from SharePoint's JavaScript API in order to display the necessary content. The type of information queried includes list names, list items, user permissions and many others.

By default, these requests are executed as soon as they are registered, leading to a lot of queries being done to SharePoint at the same time which can cause some stress on SharePoint's service and lead to throttling.

By enabling this option, web parts will wait for one another to register their requests and only once they're all finished, will one big request be sent to SharePoint.
By doing these, the web parts avoid throttling by keeping a light load on SharePoint's service but because the request is so much bigger and they all have to wait for it to finish, the web parts may take longer to load.

To understand more about throttling and the issue solved by this options, please check <a href="https://docs.microsoft.com/en-us/sharepoint/dev/general-development/how-to-avoid-getting-throttled-or-blocked-in-sharepoint-online" target="_blank">this article</a>.