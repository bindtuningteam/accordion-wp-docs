1. On the Tab click on the dropdown and then **Settings**. If is the first time adding the **Alert**, you can skip this process. 

    ![settings_delete.png](../images/msteams/01.edit.png)
   
2. Configure the web part according to the settings described in the **[Web Part Properties Section](./general.md)**;

    ![configurepanel.png](../images/msteams/02.configurepanel.png)

3. The properties are saved automatically, so when you're done click to close the Properties. 