
![layout-options](../images/modern/14.layoutoptions.png)

___
### Layout
There are three different layouts you can choose from

 
 |Layout1|Layout2|Layout3|
 |----------|-------------|------|
 |![layout-1](../images/msteams/03.layout-1.png)|![layout-2](../images/msteams/03.layout-2.png)|![layout-3](../images/msteams/03.layout-3.png)|

___
### Header Text Color
The color of item's header text.

___
### Header Contrast Color
The secondary color of the item's header. This is available on Layout 1 only. The other two layouts use a calculated darker or lighter variant of the text color.

___
### Show Search
Activate this option if you want to display a search bar on top of the webpart. This search, allows users to find and filter items that contain the search expression.

___
### Transparency
If the item's content background should be transparent or not. 

___
Layout Examples:

   |Opaque|Transparent|
 |----------|-------------|
 |![opaque](../images/msteams/04.opaque.png)|![transparent](../images/msteams/04.transparent.png)|


|![search](../images/msteams/02.search.gif)