![Add section](../images/msteams/06.advanced.png)

### Title Link 

Note this option is only valid if you have a Title on the Web Part.<br>
On the **Title** of the Web Part, you can add an URL to redirect you to any page. The Web Part will always open the new URL in a new tab of your browser and that option is only visible if you're not in **Edit** mode.

___
### Title

If you click on the Title of the Web Part on the page, you can change the Title to the Web Part that will be shown. When you insert a text there it'll show on the page the **Title**, otherwise, nothing will be visible in the page.


___
### Allow HTML
 
If you activate this option, the item's content will be displayed as HTML instead of plain text. This is useful if you want to insert your custom html or an embedded widget.

<p class="alert alert-warning"><strong>Warning:</strong> Malformed HTML can lead to a broken page layout. If that happens, you may not be able to edit the web part and you need to go to the list and change (or delete) the HTML content.</p>