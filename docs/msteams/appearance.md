![webpartapperence](../images/msteams/05.appearence.png)

___
### Right to Left

Using this option will change the web part's text orientation to go from right to left. 
The forms will not be affected.