1. Open the Team on **Teams panel** that you intend to remove the Web Part; 
2. Click on the **Settings** button.

	![edit-page](../../images/msteams/01.edit.png)

2. On the web part click the **Manage Accordion** icon;
3. The list of items will appear. Click the **trash** icon to delete the item;
4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the item will be removed.

    ![delete_faq](../../images/msteams/03.delete-item.gif)