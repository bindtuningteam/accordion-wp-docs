1. Open the Team on **Teams panel** that you intend to add the Web Part; 
2. Click on the **Settings** button.

	![edit-page](../../images/msteams/01.edit.png)

2. On the web part click the **Manage Accordion** icon;
3. The list of items will appear. Click the **pencil** icon to edit the item;
4. You can check what you can edit in each section on the [Accordion Settings](../../global/faq); 

	![edit-faq](../../images/msteams/03.edit-item.gif)

5. Done editing? Click on **Save** to save your settings.