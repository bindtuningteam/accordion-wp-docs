1. Open the Team on **Teams panel** that you intend to add the Web Part; 
2. Click on the **Settings** button. 

	![edit-page](../../images/msteams/01.edit.png)


2. Mouse hover the Web Part and click on the **[+]** button to add a new **Accordion's item**;

	![add_faq](../../images/msteams/03.add-item.gif)
	

3. Fill out the form that pops up. You can check out what you need to do in each setting in the [Accordion Settings](../../global/faq)

4. After setting everything up, click on **Save**. 