![options](../images/msteams/02.configurepanel.png)

On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Create New Accordion's List](../createlist)
- [Sources](../list)
- [Layout Options](../layoutoptions)
- [Web Part Appearance](../appearance)
- [Advanced Options](../advanced)
- [Performance](../performance)
- [Web Part Messages](../message)